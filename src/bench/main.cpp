#include "../locks/clh_lock.h"
#include "../locks/mcs_lock.h"
#include "../locks/tas_lock.h"
#include "../locks/ticket_lock.h"
#include "../helpers/helpers.h"

// #include <sched.h>

#include <atomic>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <mutex>
#include <numeric>
#include <sstream>
#include <thread>
#include <vector>

struct LockUnlockTimings {
  uint64_t start_acquire;
  uint64_t finish_acquire;
  uint64_t start_release;
  uint64_t finish_release;

  int thread_id;
  int iteration_id;
};

struct Stats {
  std::vector<double> durations;
  std::vector<LockUnlockTimings> all_timings;
  std::mutex mtx;

  void add_timings(const LockUnlockTimings& timings);
};

void Stats::add_timings(const LockUnlockTimings& timings) {
  std::lock_guard<std::mutex> lock(mtx);
  all_timings.push_back(timings);
}

void print_stats(const Stats& stats, int threads_count, const std::string& lock_type, double duration_of_tick) {
  auto median = calc_median(stats.durations);

  auto bound = median * 3;
  std::cout << static_cast<int64_t>(calc_average_with_filter(stats.durations, bound) * duration_of_tick) << ";"
      << static_cast<int64_t>(calc_variance_with_filter(stats.durations, bound) * duration_of_tick) << ";";

  {
    std::ofstream fs("all_timings_" + lock_type + "_" + std::to_string(threads_count) + ".csv");
    fs << "thread_id;iteration_id;start_acquire;finish_acquire;start_release;finish_release" << std::endl;

    for (const auto& timings : stats.all_timings) {
      fs << timings.thread_id << ";" << timings.iteration_id << ";" << static_cast<int64_t>(duration_of_tick * timings.start_acquire) << ";"
        << static_cast<int64_t>(duration_of_tick * timings.finish_acquire) << ";"
        << static_cast<int64_t>(duration_of_tick * timings.start_release) << ";"
        << static_cast<int64_t>(duration_of_tick * timings.finish_release) << std::endl;
    }
  }

  {
    std::ofstream fs("all_durations_" + lock_type + "_" + std::to_string(threads_count) + ".csv");
    int i = 0;
    for (const auto& d : stats.durations) {
      ++i;
      fs << i << ") " << d << std::endl;
    }
  }
}

template <typename LockType>
LockUnlockTimings release_acquire_lock(
    LockType& lock, std::atomic<int>& finished_threads_count) {
  LockUnlockTimings timings;

  timings.start_acquire = ticks();
  lock.acquire();
  timings.finish_acquire = ticks();
  asm volatile("":::"memory");
  timings.start_release = ticks();
  lock.release();
  timings.finish_release = ticks();

  finished_threads_count.fetch_add(1);
  return timings;
}

template <>
LockUnlockTimings release_acquire_lock<MCSLock>(
    MCSLock& lock, std::atomic<int>& finished_threads_count) {
  MCSLock::Node thread_node{};
  LockUnlockTimings timings;

  timings.start_acquire = ticks();
  lock.acquire(&thread_node);
  timings.finish_acquire = ticks();
  asm volatile("":::"memory");
  timings.start_release = ticks();
  lock.release(&thread_node);
  timings.finish_release = ticks();

  finished_threads_count.fetch_add(1);
  return timings;
}

template <>
LockUnlockTimings release_acquire_lock<CLHLock>(
    CLHLock& lock, std::atomic<int>& finished_threads_count) {
  // TODO
  return {};
}

template <typename LockType>
void thread_measures(
    std::atomic<int>& iter_num,
    std::atomic<int>& finished_threads_count,
    std::atomic<int>& all_finished_threads_count,
    Stats& stats,
    int thread_id,
    int iterations_count,
    LockType& lock) {
  for (int iteration_id = 1; iteration_id <= iterations_count; ++iteration_id) {
    // wait start of iteration
    while (iter_num.load() != iteration_id) {}

    auto timings = release_acquire_lock(lock, finished_threads_count);

    timings.thread_id = thread_id;
    timings.iteration_id = iteration_id;

    stats.add_timings(timings);

    all_finished_threads_count.fetch_add(1);
  }
}

template <typename LockType>
void measure_iterations(int threads_count, const std::string& lock_type, double duration_of_tick) {
  const int iterations_count = 1000;

  std::atomic<int> iter_num{0};
  std::atomic<int> finished_threads_count{0};
  std::atomic<int> all_finished_threads_count{0};

  std::vector<std::thread> threads;
  threads.reserve(threads_count);

  Stats stats;
  stats.all_timings.reserve(threads_count * iterations_count);
  stats.durations.reserve(iterations_count);

  LockType lock;

  for (int thread_id = 0; thread_id < threads_count; ++thread_id) {
    threads.emplace_back(
      [&iter_num, &finished_threads_count, &all_finished_threads_count, &stats, &lock, thread_id]() {
          thread_measures<LockType>(iter_num, finished_threads_count, all_finished_threads_count,
              stats, thread_id, iterations_count, lock);
      });
    /* cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(thread_id, &cpuset);
    int rc = pthread_setaffinity_np(threads[thread_id].native_handle(),
                                    sizeof(cpu_set_t), &cpuset);
    if (rc != 0) {
      std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
    } */
  }

  while (iter_num.load() < iterations_count) {
    auto start = ticks();
    iter_num.fetch_add(1);
    while (finished_threads_count.load() != threads_count) {}
    auto finish = ticks();

    while (all_finished_threads_count.load() != threads_count) {}
    finished_threads_count.store(0);
    all_finished_threads_count.store(0);

    double iteration_duration = finish - start;
    stats.durations.push_back(iteration_duration / threads_count);
  }

  clear_after_measure(threads);
  print_stats(stats, threads_count, lock_type, duration_of_tick);
}

int main(int argc, char** argv) {
  if (argc < 4) {
    throw std::runtime_error("No arguments");
  }

  std::string lock_type(argv[1]);

  int threads_count;
  {
    std::stringstream ss(argv[2]);
    ss >> threads_count;
  }

  double duration_of_tick;
  {
    std::stringstream ss(argv[3]);
    ss >> duration_of_tick;
  }

  if (lock_type == "tas") {
    measure_iterations<TASLock>(threads_count, lock_type, duration_of_tick);
  } else if (lock_type == "mcs") {
    measure_iterations<MCSLock>(threads_count, lock_type, duration_of_tick);
  } else if (lock_type == "ticket") {
    measure_iterations<TicketLock>(threads_count, lock_type, duration_of_tick);
  } else if (lock_type == "clh") {
    // measure_iterations<CLHLock>(threads_count);
  }

  return 0;
}
