#include "clh_lock.h"

void CLHLock::acquire(Node** thread_node) {
  (*thread_node)->succ_must_wait = true;
  Node* prev = tail.exchange(*thread_node);
  while (prev->succ_must_wait.load()) {
  }

  head.store(*thread_node);
  *thread_node = prev;
}

void CLHLock::release() {
  head.load()->succ_must_wait.store(false);
}
