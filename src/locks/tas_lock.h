#pragma once

#include <atomic>
#include <thread>

class TASLock {
public:
  __attribute__((always_inline)) void acquire();
  __attribute__((always_inline)) void release();

private:
  std::atomic<bool> flag{false};
};

void TASLock::acquire() {
  bool expected = false;
  while (!flag.compare_exchange_strong(expected, true)) {
    while (flag.load()) {}
    expected = false;
  }
}

void TASLock::release() {
  flag.store(false);
}
