#pragma once

#include <atomic>

class TicketLock {
public:
  __attribute__((always_inline)) void acquire();
  __attribute__((always_inline)) void release();

private:
  std::atomic<int> next_ticket{0};
  std::atomic<int> now_serving{0};
};

void TicketLock::acquire() {
  auto my_ticket = next_ticket.fetch_add(1);
  while (now_serving.load() != my_ticket) {}
}

void TicketLock::release() {
  now_serving.fetch_add(1);  // TODO: ??
}
