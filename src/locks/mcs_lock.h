#pragma once

#include <atomic>

class MCSLock {
public:
  struct Node {
    std::atomic<bool> waiting{true};
    std::atomic<Node*> next{nullptr};
  };

public:
  __attribute__((always_inline)) void acquire(Node* thread_node);
  __attribute__((always_inline)) void release(Node* thread_node);

private:
  std::atomic<Node*> tail{nullptr};
};

void MCSLock::acquire(Node* thread_node) {
  thread_node->next = nullptr;
  thread_node->waiting = true;

  auto prev_node = tail.exchange(thread_node); // std::memory_order_acquire) TODO
  if (prev_node != nullptr) {
      thread_node->waiting = true;
      prev_node->next = thread_node;
      while (thread_node->waiting) {}
      // asm volatile("pause\n": : :"memory"); TODO
  }
}

void MCSLock::release(Node* thread_node) {
  if (thread_node->next == nullptr) {
    Node* p = thread_node;
    if (tail.compare_exchange_strong(p, nullptr)) {
        // TODO std::memory_order_release, std::memory_order_relaxed)) {
      return;
    }
    while (thread_node->next == nullptr) {}
  }

  thread_node->next.load()->waiting = false;
  thread_node->next = nullptr;
}
