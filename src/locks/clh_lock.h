#pragma once

#include <atomic>

class CLHLock {
public:
  struct Node {
    std::atomic<bool> succ_must_wait{false};
  };

public:
  void acquire(Node** thread_node);
  void release();

private:
  Node dummy{};
  std::atomic<Node*> tail{&dummy};
  std::atomic<Node*> head;
};
