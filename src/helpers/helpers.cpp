#include "helpers.h"

void clear_after_measure(std::vector<std::thread>& threads) {
  for (auto& task : threads) {
    task.join();
  }
}
