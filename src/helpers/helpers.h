#pragma once

#include <algorithm>
#include <cmath>
#include <vector>
#include <mutex>
#include <numeric>
#include <fstream>
#include <thread>
#include <iostream>

void clear_after_measure(std::vector<std::thread>& threads);

template <typename T>
double calc_average(const std::vector<T>& durations) {
  return std::accumulate(durations.begin(), durations.end(), 0.0) / durations.size();
}

template <typename T>
double calc_median(const std::vector<T>& durations) {
  std::vector<T> tmp(durations);
  std::sort(tmp.begin(), tmp.end());
  return tmp[tmp.size() / 2];
}

template <typename T>
double calc_average_with_filter(const std::vector<T>& durations, T bound) {
  double result = 0;
  int count = 0;
  for (const auto& elem : durations) {
    if (elem > bound) {
      continue;
    }
    ++count;
    result += elem;
  }
  return result / count;
}

template <typename T>
double calc_variance(const std::vector<T>& durations) {
  double average = calc_average(durations);
  double result = 0;

  for (const auto& elem : durations) {
    result += (elem - average) * (elem - average);
  }
  result /= durations.size();
  return std::sqrt(result);
}

template <typename T>
double calc_variance_with_filter(const std::vector<T>& durations, T bound) {
  double average = calc_average_with_filter(durations, bound);
  double result = 0;
  int count = 0;

  for (const auto& elem : durations) {
    if (elem > bound) {
      continue;
    }
    result += (elem - average) * (elem - average);
    ++count;
  }
  result /= count;
  return std::sqrt(result);
}

inline uint64_t ticks() {
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}
