#include "../locks/clh_lock.h"
#include "../locks/mcs_lock.h"
#include "../locks/tas_lock.h"
#include "../locks/ticket_lock.h"
#include "../helpers/helpers.h"

// #include <sched.h>

#include <atomic>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <mutex>
#include <numeric>
#include <sstream>
#include <thread>
#include <vector>

struct Stats {
  std::atomic<int> locks_count{0};
  std::chrono::microseconds duration;

  void print(int threads_count) {
    auto rps = static_cast<double>(locks_count.load()) * (static_cast<double>(1000000) / duration.count());
    std::cout << static_cast<int64_t>(rps) << ";";
  }
};

template <typename LockType>
void release_acquire_lock(LockType& lock) {
  lock.acquire();
  asm volatile("":::"memory");
  lock.release();
}

template <>
void release_acquire_lock<MCSLock>(MCSLock& lock) {
  MCSLock::Node thread_node{};

  lock.acquire(&thread_node);
  asm volatile("":::"memory");
  lock.release(&thread_node);
}

template <>
void release_acquire_lock<CLHLock>(CLHLock& /*lock*/) {
  // TODO
}

template <typename LockType>
void thread_measures(
    Stats& stats,
    int /*thread_id*/,
    std::atomic<bool>& can_start,
    std::atomic<int>& finish_flag,
    LockType& lock) {
  int locks_count = 0;

  while (!can_start.load()) {}
  while (!finish_flag.load()) {
    release_acquire_lock(lock);
    locks_count++;
  }

  stats.locks_count.fetch_add(locks_count);
}

template <typename LockType>
void measure_iterations(int threads_count) {
  const auto duration = std::chrono::microseconds{10'000};

  std::atomic<bool> can_start{false};
  std::atomic<int> finish_flag{0};

  std::vector<std::thread> threads;
  threads.reserve(threads_count);

  Stats stats;

  LockType lock;

  for (int thread_id = 0; thread_id < threads_count; ++thread_id) {
    threads.emplace_back(
      [thread_id, &stats, &can_start, &lock, &finish_flag]() {
          thread_measures<LockType>(stats, thread_id, can_start, finish_flag, lock);
      });

    /*cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(thread_id, &cpuset);
    int rc = pthread_setaffinity_np(threads[thread_id].native_handle(),
                                    sizeof(cpu_set_t), &cpuset);
    if (rc != 0) {
      std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
    }*/
  }

  std::this_thread::sleep_for(std::chrono::seconds(1));

  can_start.store(true);
  std::this_thread::sleep_for(duration);
  finish_flag.store(1);

  clear_after_measure(threads);

  stats.duration = duration;

  stats.print(threads_count);
}

int main(int argc, char** argv) {
  if (argc < 3) {
    throw std::runtime_error("No arguments");
  }

  std::string lock_type(argv[1]);

  int threads_count;
  {
    std::stringstream ss(argv[2]);
    ss >> threads_count;
  }

  if (lock_type == "tas") {
    measure_iterations<TASLock>(threads_count);
  } else if (lock_type == "mcs") {
    measure_iterations<MCSLock>(threads_count);
  } else if (lock_type == "ticket") {
    measure_iterations<TicketLock>(threads_count);
  } else if (lock_type == "clh") {
    // measure_iterations<CLHLock>(threads_count);
  }

  return 0;
}
