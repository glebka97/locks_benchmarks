#include "../helpers/helpers.h"

#include <iostream>
#include <chrono>

int main() {
  auto duration = std::chrono::nanoseconds(1'000'000'000);
  auto start_ticks = ticks();

  std::this_thread::sleep_for(duration);

  auto finish_ticks = ticks();

  auto ns_count = duration.count();
  auto ticks_count = finish_ticks - start_ticks;
  std::cerr << "Ticks: " << ticks_count << ", duration: " << ns_count << std::endl;

  std::cerr << "Duration of one tick (in ns): " << std::fixed << ((double)ns_count / ticks_count) << std::endl;

  std::cout << std::fixed << ((double)ns_count / ticks_count) << std::endl;
  return 0;
};
