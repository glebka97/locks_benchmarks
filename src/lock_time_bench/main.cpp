#include "../locks/clh_lock.h"
#include "../locks/mcs_lock.h"
#include "../locks/tas_lock.h"
#include "../locks/ticket_lock.h"
#include "../helpers/helpers.h"

// #include <sched.h>

#include <atomic>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <mutex>
#include <numeric>
#include <sstream>
#include <thread>
#include <vector>

struct LockUnlockTimings {
  uint64_t start_acquire;
  uint64_t finish_acquire;
  uint64_t start_release;
  uint64_t finish_release;
  int thread_id;
};

struct RealActionTiming {
  uint64_t duration;
  int thread_id;
};

enum class ActionType {
  kStartAcquire,
  kStartRelease,
  kFinishRelease,
  kFinishAcquire,
};

struct Action {
  uint64_t ticks;
  ActionType type;
  int thread_id;
};

std::string to_string(ActionType type) {
  switch (type) {
    case ActionType::kStartAcquire: return "start_acquire";
    case ActionType::kFinishAcquire: return "finish_acquire";
    case ActionType::kStartRelease: return "start_release";
    case ActionType::kFinishRelease: return "finish_release";
  }
}

struct Stats {
  std::vector<LockUnlockTimings> all_timings;
  std::vector<Action> actions;
  std::vector<RealActionTiming> real_lock_timings;
  std::vector<RealActionTiming> real_unlock_timings;
  std::map<int, uint64_t> thread_locks_count;
  std::mutex mtx;

  void add_timings(const std::vector<LockUnlockTimings>& timings) {
    std::lock_guard<std::mutex> lock(mtx);
    std::copy(timings.begin(), timings.end(), std::back_inserter(all_timings));
  }

  void calc_real_lock_timings() {
    size_t pos = 0;
    while (pos < actions.size()) {
      for (; pos < actions.size() && actions[pos].type != ActionType::kStartRelease; ++pos) {}

      if (pos >= actions.size()) {
        break;
      }

      uint64_t start_locking_pos = pos;
      uint64_t start_locking = actions[pos].ticks;

      for (; pos < actions.size() && actions[pos].type != ActionType::kFinishAcquire; ++pos) {}

      if (pos >= actions.size()) {
        break;
      }

      // check if StartAcquire before FinishRelease
      bool ok = true;
      for (size_t i = pos; i >= start_locking_pos; --i) {
        if (actions[i].thread_id == actions[pos].thread_id && actions[i].type == ActionType::kStartAcquire) {
          ok = false;
          break;
        }
      }

      if (!ok) {
        continue;
      }

      uint64_t finish_locking = actions[pos].ticks;

      real_lock_timings.push_back({finish_locking - start_locking, actions[pos].thread_id});
    }
  }

  void calc_real_unlock_timings() {
    size_t pos = 0;
    while (pos < actions.size()) {
      for (; pos < actions.size() && actions[pos].type != ActionType::kStartRelease; ++pos) {}

      if (pos >= actions.size()) {
        break;
      }

      uint64_t start_unlocking = actions[pos].ticks;

      for (; pos < actions.size() && actions[pos].type != ActionType::kFinishRelease; ++pos) {}

      if (pos >= actions.size()) {
        break;
      }

      uint64_t finish_unlocking = actions[pos].ticks;

      real_unlock_timings.push_back({finish_unlocking - start_unlocking, actions[pos].thread_id});
    }
  }

  void calc_fairness() {
    for (const auto& action : actions) {
      if (action.type == ActionType::kFinishAcquire) {
        ++thread_locks_count[action.thread_id];
      }
    }
  }

  void calc() {
    actions.reserve(all_timings.size() * 4);

    for (const auto& timings : all_timings) {
      actions.push_back({timings.start_acquire, ActionType::kStartAcquire, timings.thread_id});
      actions.push_back({timings.finish_acquire, ActionType::kFinishAcquire, timings.thread_id});
      actions.push_back({timings.start_release, ActionType::kStartRelease, timings.thread_id});
      actions.push_back({timings.finish_release, ActionType::kFinishRelease, timings.thread_id});
    }

    std::sort(actions.begin(), actions.end(), [](const Action& lhs, const Action& rhs) {
        if (lhs.ticks != rhs.ticks) {
            return lhs.ticks < rhs.ticks;
        }
        if (lhs.type != rhs.type) {
          return static_cast<int>(lhs.type) < static_cast<int>(rhs.type);
        }
        return lhs.thread_id < rhs.thread_id;
    });

    calc_real_lock_timings();
    calc_real_unlock_timings();
    calc_fairness();
  }

  std::vector<double> convert(const std::vector<RealActionTiming> real_action_timings) {
    std::vector<double> action_ticks;
    action_ticks.reserve(real_lock_timings.size());
    std::transform(real_action_timings.begin(), real_action_timings.end(), std::back_inserter(action_ticks),
        [](const RealActionTiming& timing) { return timing.duration; } );

    return action_ticks;
  }

  void print(int threads_count, const std::string& lock_type, double duration_of_tick) {
    /*{
      std::ofstream fs("all_timings_2_" + lock_type + "_" + std::to_string(threads_count) + ".csv");
      for (const auto&
    }*/
    {
      std::ofstream fs("actions_" + lock_type + "_" + std::to_string(threads_count) + ".csv");
      for (const auto& action : actions) {
        fs << action.thread_id << " " << action.ticks << " " << to_string(action.type) << std::endl;
      }
    }

    {
      std::ofstream fs("lock_times_" + lock_type + "_" + std::to_string(threads_count) + ".csv");
      for (const auto& real_lock_timing : real_lock_timings) {
        fs << real_lock_timing.thread_id << ";" << real_lock_timing.duration << std::endl;
      }
    }

    {
      std::ofstream fs("unlock_times_" + lock_type + "_" + std::to_string(threads_count) + ".csv");
      for (const auto& real_unlock_timing : real_unlock_timings) {
        fs << real_unlock_timing.thread_id << ";" << real_unlock_timing.duration << std::endl;
      }
    }

    {
      std::ofstream fs("thread_locks_count_" + lock_type + "_" + std::to_string(threads_count) + ".csv");
      for (const auto& [thread_id, count] : thread_locks_count) {
        fs << "thread_id: " << thread_id << ", count: " << count << std::endl;
      }

      std::vector<int> counts;
      counts.reserve(thread_locks_count.size());
      std::transform(thread_locks_count.begin(), thread_locks_count.end(),
          std::back_inserter(counts),
          [](const auto& elem) { return elem.second; });

      fs << "Average: " << calc_average(counts) << ", variance: " << calc_variance(counts) << std::endl;
  }

    auto lock_ticks = convert(real_lock_timings);
    auto unlock_ticks = convert(real_unlock_timings);

    double lock_bound = calc_median(lock_ticks) * 3;
    double unlock_bound = calc_median(unlock_ticks) * 3;

    std::cout << static_cast<int64_t>(calc_average_with_filter(lock_ticks, lock_bound) * duration_of_tick) << ";"
      << static_cast<int64_t>(calc_variance_with_filter(lock_ticks, lock_bound) * duration_of_tick) << ";"
      << static_cast<int64_t>(calc_average_with_filter(unlock_ticks, unlock_bound) * duration_of_tick) << ";"
      << static_cast<int64_t>(calc_variance_with_filter(unlock_ticks, unlock_bound) * duration_of_tick) << ";";
  }
};


template <typename LockType>
LockUnlockTimings release_acquire_lock(LockType& lock) {
  LockUnlockTimings timings;

  timings.start_acquire = ticks();
  lock.acquire();
  timings.finish_acquire = ticks();
  asm volatile("":::"memory");
  timings.start_release = ticks();
  lock.release();
  timings.finish_release = ticks();

  return timings;
}

template <>
LockUnlockTimings release_acquire_lock<MCSLock>(MCSLock& lock) {
  MCSLock::Node thread_node{};
  LockUnlockTimings timings;

  timings.start_acquire = ticks();
  lock.acquire(&thread_node);
  timings.finish_acquire = ticks();
  asm volatile("":::"memory");
  timings.start_release = ticks();
  lock.release(&thread_node);
  timings.finish_release = ticks();

  return timings;
}

template <>
LockUnlockTimings release_acquire_lock<CLHLock>(CLHLock& lock) {
  // TODO
  return {};
}

template <typename LockType>
void thread_measures(
    Stats& stats,
    int thread_id,
    std::atomic<int>& finish_flag,
    std::atomic<bool>& can_start,
    LockType& lock) {
  std::vector<LockUnlockTimings> all_timings;
  all_timings.reserve(1000);

  while (!can_start.load()) {}
  while (!finish_flag.load()) {
    auto timings = release_acquire_lock(lock);
    timings.thread_id = thread_id;

    all_timings.push_back(timings);
  }

  stats.add_timings(all_timings);
}

template <typename LockType>
void measure_iterations(int threads_count, const std::string& lock_type, double duration_of_tick) {
  const auto duration = std::chrono::microseconds{10'000};

  std::atomic<bool> can_start{false};
  std::atomic<int> finish_flag{0};

  std::vector<std::thread> threads;
  threads.reserve(threads_count);

  Stats stats;
  stats.all_timings.reserve(threads_count * 1000);  // TODO: change

  LockType lock;

  for (int thread_id = 0; thread_id < threads_count; ++thread_id) {
    threads.emplace_back(
      [thread_id, &stats, &finish_flag, &can_start, &lock]() {
          thread_measures<LockType>(stats, thread_id, finish_flag, can_start, lock);
      });

    /*cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(thread_id, &cpuset);
    int rc = pthread_setaffinity_np(threads[thread_id].native_handle(),
                                    sizeof(cpu_set_t), &cpuset);
    if (rc != 0) {
      std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
    }*/
  }

  std::this_thread::sleep_for(std::chrono::seconds(1));

  can_start.store(true);
  std::this_thread::sleep_for(duration);
  finish_flag.store(1);

  clear_after_measure(threads);

  stats.calc();
  stats.print(threads_count, lock_type, duration_of_tick);
}

int main(int argc, char** argv) {
  if (argc < 3) {
    throw std::runtime_error("No arguments");
  }

  std::string lock_type(argv[1]);

  int threads_count;
  {
    std::stringstream ss(argv[2]);
    ss >> threads_count;
  }

  double duration_of_tick;
  {
    std::stringstream ss(argv[3]);
    ss >> duration_of_tick;
  }

  if (lock_type == "tas") {
    measure_iterations<TASLock>(threads_count, lock_type, duration_of_tick);
  } else if (lock_type == "mcs") {
    measure_iterations<MCSLock>(threads_count, lock_type, duration_of_tick);
  } else if (lock_type == "ticket") {
    measure_iterations<TicketLock>(threads_count, lock_type, duration_of_tick);
  } else if (lock_type == "clh") {
    // measure_iterations<CLHLock>(threads_count);
  }

  return 0;
}
