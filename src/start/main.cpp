#include <atomic>
#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

int main(int /*argc*/, char** argv) {
  int threads_count;
  std::stringstream in(argv[1]);
  in >> threads_count;
  threads_count *= 2;

  std::vector<std::thread> threads;
  threads.reserve(threads_count);

  std::atomic<unsigned int> cnt{0};

  std::atomic<int> stop_flag{0};
  for (int i = 0; i < threads_count; ++i) {
    threads.emplace_back([&cnt, &stop_flag](){
      unsigned int x = 1;
      while (stop_flag.load() == 0) {
        x *= 2;
        ++x;
      }
      cnt.fetch_add(x);
    });
  }

  std::this_thread::sleep_for(std::chrono::seconds(10));
  stop_flag.store(1);

  for (auto& thread : threads) {
    thread.join();
  }

  std::cerr << cnt.load() << std::endl;

  return 0;
}
