build:
	mkdir -p build && cd build && cmake .. && make bench && make lock_time_bench && make rps_bench && make ticks && cd ..

benchmark_1:
	mkdir -p tmp && cd tmp && python3 ../scripts/bench.py > stat.csv && cat stat.csv && cd ..

benchmark_2:
	mkdir -p tmp && cd tmp && python3 ../scripts/lock_time_bench.py > lock_time_stat.csv && cat lock_time_stat.csv && cd ..

benchmark_rps:
	mkdir -p tmp && cd tmp && python3 ../scripts/rps_bench.py > rps_stat.csv && cat rps_stat.csv && cd ..

graph_1:
	cd tmp && python3 ../scripts/graph.py && cd ..

graph_2:
	cd tmp && python3 ../scripts/lock_time_graph.py && cd ..

graph_rps:
	cd tmp && python3 ../scripts/rps_graph.py && cd ..
