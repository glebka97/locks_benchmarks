import os
import sys

min_threads_count = 2
max_threads_count = 62

os.system('../build/ticks/ticks > ../tmp/duration_of_tick.txt')

with open("../tmp/duration_of_tick.txt") as f:
    duration_of_tick = f.read().replace('\n', '')

threads_counts = range(min_threads_count, max_threads_count + 1, 2)
lock_types = ["tas", "ticket", "mcs"] # , "clh"]

head = "threads_count;"
for lock_type in lock_types:
    head += f'{lock_type}_lock_average;{lock_type}_lock_variance;'
    head += f'{lock_type}_unlock_average;{lock_type}_unlock_variance;'

print(head)

print('Start', end='', file=sys.stderr)
for threads_count in threads_counts:
    print(f'{threads_count};', end='', flush=True)
    for lock_type in lock_types:
        print("\rThreads_count: ", threads_count, ", type: ", lock_type, "     ", sep='', end='', file=sys.stderr)
        os.system(f'../build/lock_time_bench/lock_time_bench {lock_type} {threads_count} {duration_of_tick} 2> /dev/null')
    print('', flush=True)
print("\rFinished                                             ", file=sys.stderr)
