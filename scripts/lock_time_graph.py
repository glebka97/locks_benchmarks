import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('../tmp/lock_time_stat.csv', delimiter=';')

df.plot(
    x="threads_count",
    y=["tas_lock_average", "ticket_lock_average", "mcs_lock_average"]
)

plt.show()
