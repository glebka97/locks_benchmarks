import matplotlib.pyplot as plt
import random
import numpy as np
import string
import pandas as pd
import sys
from argparse import ArgumentParser

def find_min(df):
    res = df[0]['start_acquire']
    for event in df:
        if event['start_acquire'] < res:
            res = event['start_acquire']
    return res

def get_finish_acquire(elem):
    return elem.get('finish_acquire')

def to_xy(df, start_label, finish_label):
    min_ts = find_min(df)

    thread_id_to_events = {}
    for event in df:
        thread_id = event['thread_id']
        if thread_id not in thread_id_to_events:
            thread_id_to_events[thread_id] = []
        thread_id_to_events[thread_id] += [event]

    x, y = [], []
    for thread_id, events in thread_id_to_events.items():
        for event in events:
            print('start_release: ', event[finish_label], ", finish_acquire: ", event[start_label])
            ts_list = [
                int(event[start_label]) - min_ts,
                int(event[finish_label]) - min_ts,
            ]
            y.extend([thread_id])
            x.extend([ts_list])

    x, y = np.array(x), np.array(y)
    return x,y

parser = ArgumentParser()
parser.add_argument("-f", "--file", dest="filename",
                    help="input file", default='../tmp/timeline.csv')
parser.add_argument("-n", "--threads-count",
                    dest="threads_count", default=10,
                    help="Threads count")

args = parser.parse_args()

df = pd.read_csv(args.filename, delimiter=';').to_dict('records')

df.sort(key=get_finish_acquire)
for i, item in enumerate(df):
    item['thread_id'] = i

lock_x, lock_y = to_xy(df, 'start_acquire', 'finish_acquire')
for x, y in zip(lock_x, lock_y):
    plt.hlines(y, x[0], x[1], lw = 2, color='orange', linestyles='dashed')

body_x, body_y = to_xy(df, 'finish_acquire', 'start_release')
for x, y in zip(body_x, body_y):
    plt.hlines(y, x[0], x[1], lw = 3, color='darkviolet')
    plt.axvline(x=x[0], color='silver', lw=1, linestyle='dashed')

unlock_x, unlock_y = to_xy(df, 'start_release', 'finish_release')
for x, y in zip(unlock_x, unlock_y):
    plt.hlines(y, x[0], x[1], lw = 2, color='royalblue')


plt.ylim(max(body_y)+0.5, min(body_y)-0.5)

threads_count = int(args.threads_count)
labels = [i for i in range(threads_count)]
plt.yticks(range(body_y.max()+1), labels)

plt.show()
