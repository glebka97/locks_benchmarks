import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('../tmp/rps_stat.csv', delimiter=';')

df.plot(
    x="threads_count",
    y=["tas_rps", "ticket_rps", "mcs_rps"]
)

plt.show()
