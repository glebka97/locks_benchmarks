import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('../tmp/stat.csv', delimiter=';')

df.plot(
    x="threads_count",
    y=["tas_average", "ticket_average", "mcs_average"]
)

plt.show()
